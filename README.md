## RabbitMQ Cluster
Uses Ansible to create customized Dockerfiles to run a RabbitMQ cluster.  
It uses RabbitMQ 3.8.9 for deploying set of 3 nodes rabbit1, rabbit2, rabbit3.
Number of nodes are configurable.

Inspired on  https://github.com/kailashyogeshwar85/docker-rabbitmq-cluster by
kailashyogeshwar85@gmail.com

## Resources

There are 3 folders.

- erlang:  It contains the Dockerfile to build erlang-alpine that will be used by RabbitMQ
- server:  It contains the Dockerfile and scripts to build RabbitMQ server.
- cluster: It contains docker-compose file to launch and manage the cluster of RabbitMQ nodes

## Usage

For running RabbitMQ Single node follow below steps
```
  ansible-playbook -l localhost playbook-create.yml
  ansible-playbook -l localhost playbook-deploy.yml
```

Visit the http://<docker-ip>:15672 to view management console

NOTE:
guest user will not be able to login to management ui as it supports only localhost

### Environment Variables

Currently all variables ara on r section of playbook-create.yml
```yaml
  vars:
    - version: "3.8.9"
    - erlang_cookie: "n5JyqjtMrHJxVRuFJwYTwqLm3w3K6Nkm"
    - erland_repo: "rabbit_erlangbase"
    - base_repo: "rabbitmq_base"
    - build_date: ansible_date_time.date
    - maintainer: "Marck Collado <mcollado@sens.solutions>"
    - server_path: "rabbitmq-server"
    - url_server: https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.9/rabbitmq-server-generic-unix-3.8.9.tar.xz
```

`ABBITMQ_DEFAULT_USER`: Set default Administrator user `default admin`
`RABBITMQ_DEFAULT_PASS` : Set password for default user `default admin`

### Launching Cluster
The cluster is started automaticaly with the deploy playbook, then it can be managed with:

```
docker-compose up -d

```

### TODO

- Import oficial RabbitMQ Docker image instead creating a custom one
- Move to externals volumes configuration files...
- Choose RabbitMQ version from variables
- Refactor and cleaning
- Integrate a Traefik service
- Add Docker tools (Portainer) in a development playbook

### Clusters Launching Progress 
![cluster](screenshots/cluster.png)

![cluster1](screenshots/cluster1.png)

### Management UI
Manage UI is accssible using localhost:15672 or http://<dockerip>:15672 i.e in my case it is http://192.168.224.1:15672. You can find docker ip using docker network inspect network_name

![Management1](screenshots/management1.png)


### Management Dashboard 
![dashboard](screenshots/admin-metrics.png)

### Compose Logs
To view logs when RabbitMQ is launched as screenshot
```
docker-compose logs -f 
// or for specific node logs
docker-compose logs -f rabbit1
```
![compose-logs](screenshots/compose-logs.png)

# Links

- RabbitMQ versions: https://www.rabbitmq.com/news.html



